package ru.kislyak.projects.dao;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.File;
import ru.kislyak.projects.pojos.Project;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class FileDAOImpl implements FileDAO{
    private SessionFactory sessionFactory;
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public int save(File file){
        return (Integer) getSession().save(file);
    }

    @Transactional
    public void delete(File file) {
        getSession().delete(file);
    }

    @Transactional(readOnly = true)
    public List<File> getFilesByProject(Project project){
        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<File> query = cb.createQuery(File.class);
        Root<File> root = query.from(File.class);
        query.select(root).where(cb.equal(root.get("project"), project));
        Query<File> q = getSession().createQuery(query);
        return q.list();
    }

    @Transactional
    public void update(File file) {
        getSession().update(file);
    }

    @Transactional(readOnly = true)
    public File getById(Integer id) {
        File file = getSession().get(File.class, id);
        Hibernate.initialize(file.getContents());
        return file;
    }

    public List<File> getAll() {
        return null;
    }

    public void deleteById(Integer id) {
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
