package ru.kislyak.projects.dao;

import ru.kislyak.projects.pojos.Role;

public interface RoleDAO extends AbstractDAO<Integer, Role> {
    Role getRoleByRolename(String rolename);
}
