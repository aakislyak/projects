package ru.kislyak.projects.dao;

import ru.kislyak.projects.pojos.Uuid;

public interface UuidDAO extends AbstractDAO<Integer, Uuid> {
    Uuid getByUuid(String uuid);
    void deleteUuidByUsername(String username);
}
