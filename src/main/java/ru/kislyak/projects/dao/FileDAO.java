package ru.kislyak.projects.dao;

import ru.kislyak.projects.pojos.File;
import ru.kislyak.projects.pojos.Project;

import java.util.List;

public interface FileDAO extends AbstractDAO<Integer, File> {
    List<File> getFilesByProject(Project project);
}
