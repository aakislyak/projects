package ru.kislyak.projects.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.Designer;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class DesignerDAOImpl implements DesignerDAO{

    private SessionFactory sessionFactory;

    @Transactional
    public void delete(Designer designer) {
        getSession().delete(designer);
    }

    @Transactional (readOnly = true)
    public Designer getById(Integer id) {
        return getSession().get(Designer.class, id);
    }

    @Transactional (readOnly = true)
    public List<Designer> getAll() {
        CriteriaQuery<Designer> query = getSession().getCriteriaBuilder().createQuery(Designer.class);
        Root<Designer> root = query.from(Designer.class);
        query.select(root);
        Query<Designer> q = getSession().createQuery(query);
        return q.list();
    }

    @Transactional
    public int save(Designer designer) {
        return (Integer)getSession().save(designer);
    }

    @Transactional
    public void update(Designer designer) {
        getSession().update(designer);
    }

    @Transactional
    public void deleteById(Integer id) {
        getSession().delete(getById(id));
    }

    private Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}