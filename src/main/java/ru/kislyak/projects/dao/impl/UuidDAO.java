package ru.kislyak.projects.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.Uuid;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UuidDAO implements ru.kislyak.projects.dao.UuidDAO {
    private SessionFactory sessionFactory;
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }


    @Transactional
    public int save(Uuid uuid) {
        return (Integer) getSession().save(uuid);
    }

    @Override
    public Uuid getById(Integer integer) {
        return null;
    }

    @Transactional
    public List<Uuid> getAll() {
        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<Uuid> query = cb.createQuery(Uuid.class);
        Root<Uuid> root = query.from(Uuid.class);
        root.fetch("user", JoinType.LEFT);
        Query<Uuid> q = getSession().createQuery(query);
        return q.list();
    }

    @Override
    public void update(Uuid uuid) {

    }

    @Transactional
    public void delete(Uuid uuid) {
        uuid.setUser(null);
        getSession().delete(uuid);
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Transactional(readOnly = true)
    public Uuid getByUuid(String uuid) {
        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<Uuid> query = cb.createQuery(Uuid.class);
        Root<Uuid> root = query.from(Uuid.class);
        root.fetch("user", JoinType.LEFT);
        query.select(root).where(cb.equal(root.get("uuid"), uuid ));
        Query<Uuid> q = getSession().createQuery(query);
        return q.getSingleResult();
    }

    @Transactional
    public void deleteUuidByUsername(String username){
        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<Uuid> query = cb.createQuery(Uuid.class);
        Root<Uuid> root = query.from(Uuid.class);
        root.fetch("user", JoinType.LEFT);
        query.select(root).where(cb.equal(root.get("user").get("name"), username ));
        Query<Uuid> q = getSession().createQuery(query);
        List<Uuid> uuidEntitiesForDelete = q.list();
        uuidEntitiesForDelete.forEach(ue -> delete(ue));
    }
}
