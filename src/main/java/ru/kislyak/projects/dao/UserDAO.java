package ru.kislyak.projects.dao;

import ru.kislyak.projects.pojos.User;

public interface UserDAO extends AbstractDAO<Integer, User> {
    User getByUsername(String username);
}
