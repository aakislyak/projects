package ru.kislyak.projects.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.Role;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class RoleDAOImpl implements RoleDAO {

    private SessionFactory sessionFactory;

    @Transactional(readOnly = true)
    public List<Role> getAll() {
        CriteriaQuery<Role> query = getSession().getCriteriaBuilder().createQuery(Role.class);
        Root<Role> root = query.from(Role.class);
        query.select(root);
        Query<Role> q = getSession().createQuery(query);
        return q.list();
    }

    @Transactional(readOnly = true)
    public Role getRoleByRolename(String rolenameVal){
        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<Role> query = cb.createQuery(Role.class);
        Root<Role> root = query.from(Role.class);
        query.select(root).where(cb.equal(root.get("rolename"), rolenameVal));
        Query<Role> q = getSession().createQuery(query);
        return q.getSingleResult();
    }

    @Override
    public Role getById(Integer integer) {
        return null;
    }

    @Override
    public int save(Role role) { return 0;

    }

    @Override
    public void update(Role role) {

    }

    @Override
    public void delete(Role role) {

    }

    @Override
    public void deleteById(Integer integer) {

    }
    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
