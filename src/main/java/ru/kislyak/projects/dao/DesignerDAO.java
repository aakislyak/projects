package ru.kislyak.projects.dao;

import ru.kislyak.projects.pojos.Designer;

public interface DesignerDAO extends AbstractDAO<Integer, Designer> {

}
