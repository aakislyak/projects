package ru.kislyak.projects.dao;

import java.util.List;

public interface AbstractDAO<Id, Entity> {
    Entity getById(Id id);

    List<Entity> getAll();

    int save(Entity entity);

    void update(Entity entity);

    void delete(Entity entity);

    void deleteById(Id id);

}
