package ru.kislyak.projects.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.User;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {
    
    @Transactional(readOnly = true)
    public User getById(Integer id) {
        return getSession().get(User.class, id);
    }

    @Transactional(readOnly = true)
    public List<User> getAll() {
        CriteriaQuery<User> query = getSession().getCriteriaBuilder().createQuery(User.class);
        Root<User> root = query.from(User.class);
        root.fetch("rolename", JoinType.LEFT);
        query.select(root);
        Query<User> q = getSession().createQuery(query);
        return q.list();
    }

    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(cb.equal(root.get("name"), username ));
        Query<User> q = getSession().createQuery(query);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    public int save(User user) {
        return (Integer) getSession().save(user);
    }

    @Transactional
    public void update(User user) {
        getSession().update(user);
    }

    @Transactional
    public void delete(User user) {
        getSession().delete(user);
    }

    @Transactional
    public void deleteById(Integer id) {
        getSession().delete(getById(id));
    }


    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
