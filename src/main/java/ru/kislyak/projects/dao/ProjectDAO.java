package ru.kislyak.projects.dao;

import ru.kislyak.projects.pojos.Project;

public interface ProjectDAO extends AbstractDAO<Integer, Project> {
}
