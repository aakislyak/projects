package ru.kislyak.projects.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.Project;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ProjectDAOImpl implements ProjectDAO {
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional (readOnly = true)
    public Project getById(Integer id) {
        return getSession().get(Project.class, id);
    }

    @Transactional (readOnly = true)
    public List<Project> getAll() {

        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<Project> query = cb.createQuery(Project.class);
        Root<Project> root = query.from(Project.class);
        query.orderBy(cb.desc(root.get("id")));
        root.fetch("designer", JoinType.LEFT);
        query.select(root);
        return getSession().createQuery(query).list();
}

    @Transactional
    public int save(Project project) {
        return (Integer) getSession().save(project);
    }

    @Transactional
    public void update(Project project) {
        getSession().update(project);
    }

    @Transactional
    public void delete(Project project) {
        getSession().delete(project);
    }

    @Transactional
    public void deleteById(Integer id) {
        getSession().delete(getById(id));
    }

    private Session getSession(){
        return sessionFactory.getCurrentSession();
    }

}
