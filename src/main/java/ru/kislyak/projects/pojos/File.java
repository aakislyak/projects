package ru.kislyak.projects.pojos;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "FILES")
public class File implements Serializable {
    private int id;
    private String fname;
    private Long fsize;
    private String contenttype;
    private String note;
    private FilesBinData contents;
    private Project project;


    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ID_PROJECT", unique = true)
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "FNAME", nullable = true, length = 200)
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    @Basic
    @Column(name = "FSIZE", nullable = true)
    public Long getFsize() {
        return fsize;
    }

    public void setFsize(Long fsize) {
        this.fsize = fsize;
    }

    @Basic
    @Column(name = "CONTENTTYPE", nullable = true, length = 45)
    public String getContenttype() {
        return contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    @OneToOne (fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "CONTENTS")
    public FilesBinData getContents() {
        return contents;
    }

    public void setContents(FilesBinData contents) {
        this.contents = contents;
    }

    @Basic
    @Column(name = "NOTE", nullable = true, length = 200)
    public String getNote() {
        return note;
    }


    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        File that = (File) o;

        if (id != that.id) return false;
        if (fname != null ? !fname.equals(that.fname) : that.fname != null) return false;
        if (fsize != null ? !fsize.equals(that.fsize) : that.fsize != null) return false;
        if (contenttype != null ? !contenttype.equals(that.contenttype) : that.contenttype != null) return false;
        return note != null ? note.equals(that.note) : that.note == null;

    }
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (fname != null ? fname.hashCode() : 0);
        result = 31 * result + (fsize != null ? fsize.hashCode() : 0);
        result = 31 * result + (contenttype != null ? contenttype.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }

}
