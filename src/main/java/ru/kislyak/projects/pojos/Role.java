package ru.kislyak.projects.pojos;

import org.hibernate.annotations.Proxy;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Proxy(lazy=false)
@Table(name = "roles")
public class Role implements Serializable {
    private String rolename;
    private int id;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @Column(name = "rolename", nullable = false, length = 45)
    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role that = (Role) o;

        if (id != that.id) return false;
        return rolename != null ? rolename.equals(that.rolename) : that.rolename == null;

    }

    @Override
    public int hashCode() {
        int result = rolename != null ? rolename.hashCode() : 0;
        result = 31 * result + id;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Role[%d, %s]", id, rolename);
    }
}
