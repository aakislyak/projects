package ru.kislyak.projects.pojos;

import javax.faces.bean.ManagedBean;
import javax.persistence.*;

@Entity
@ManagedBean(name = "designer")
@Table(name = "DESIGNERS")
public class Designer {
    private int id;
    private String lastname;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "LASTNAME", length = 45)
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Designer that = (Designer) o;

        if (id != that.id) return false;
        return lastname.equals(that.lastname);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + lastname.hashCode();
        return result;
    }

}
