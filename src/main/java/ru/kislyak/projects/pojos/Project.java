package ru.kislyak.projects.pojos;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PROJECTS")
public class Project implements Serializable{
    private int id;
    private String objects;
    private String title;
    private String customer;
    private String invnum;
    private String libnum;
    private Boolean realised;
    private String year;
    private String note;
    private Designer designer;

    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "FK_ID_DESIGNER", unique = true)
    public Designer getDesigner() {
        return designer;
    }

    public void setDesigner(Designer designer) {
        this.designer = designer;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "OBJECTS", nullable = true, length = 45)
    public String getObjects() {
        return objects;
    }

    public void setObjects(String objects) {
        this.objects = objects;
    }

    @Basic
    @Column(name = "TITLE", nullable = true, length = 500)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "CUSTOMER", nullable = true, length = 45)
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @Basic
    @Column(name = "INVNUM", nullable = true, length = 45)
    public String getInvnum() {
        return invnum;
    }

    public void setInvnum(String invnum) {
        this.invnum = invnum;
    }

    @Basic
    @Column(name = "LIBNUM", nullable = true, length = 45)
    public String getLibnum() {
        return libnum;
    }

    public void setLibnum(String libnum) {
        this.libnum = libnum;
    }

    @Basic
    @Column(name = "REALISED", nullable = true)
    public Boolean getRealised() {
        return realised;
    }

    public void setRealised(Boolean realised) {
        this.realised = realised;
    }

    @Basic
    @Column(name = "YEAR", nullable = true, length = 45)
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "NOTE", nullable = true, length = 200)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project that = (Project) o;

        if (id != that.id) return false;
        if (objects != null ? !objects.equals(that.objects) : that.objects != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (invnum != null ? !invnum.equals(that.invnum) : that.invnum != null) return false;
        if (libnum != null ? !libnum.equals(that.libnum) : that.libnum != null) return false;
        if (realised != null ? !realised.equals(that.realised) : that.realised != null) return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        return note != null ? note.equals(that.note) : that.note == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (objects != null ? objects.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (invnum != null ? invnum.hashCode() : 0);
        result = 31 * result + (libnum != null ? libnum.hashCode() : 0);
        result = 31 * result + (realised != null ? realised.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }
}
