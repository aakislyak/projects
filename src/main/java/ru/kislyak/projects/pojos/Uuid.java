package ru.kislyak.projects.pojos;


import javax.persistence.*;

@Entity
@Table(name = "USERUID")
public class Uuid {
    private int id;
    private String uuid;
    private User user;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    @Basic
    @Column(name = "UUID", nullable = false, length = 36)
    public String getUuid() {
        return uuid;
    }

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_USER_ID", unique = true)
    public User getUser() {
        return user;
    }


    public void setId(int id) {
        this.id = id;
    }


    public void setUser(User user) {
        this.user = user;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
