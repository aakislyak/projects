package ru.kislyak.projects.pojos;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "FILESBINDATA")
public class FilesBinData implements Serializable {
    private int id;
    private byte[] binData;
    private File file;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }


    @Lob
    @Column(name = "BINDATA")
    public byte[] getBinData() {
        return binData;
    }

    @OneToOne(mappedBy="contents")
    public File getFile() {
        return file;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBinData(byte[] binData) {
        this.binData = binData;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
