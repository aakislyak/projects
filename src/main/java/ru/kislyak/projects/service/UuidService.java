package ru.kislyak.projects.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kislyak.projects.dao.UuidDAO;
import ru.kislyak.projects.pojos.Uuid;

@Service
public class UuidService {
    private UuidDAO uuidDAO;
    @Autowired
    public void setUuidDAO(UuidDAO uuidDAO) {
        this.uuidDAO = uuidDAO;
    }

    void saveUuid(Uuid uuid){
        if (uuid == null || uuid.getUser() == null) {
            return;
        }
        uuidDAO.save(uuid);
    }

    Uuid getByUuid(String uuid) {
        if (uuid==null) {
            return null;
        }
        return uuidDAO.getByUuid(uuid);
    }

    void deleteUuidByUsername(String username){
        if (username == null) {
            return;
        }
        uuidDAO.deleteUuidByUsername(username);
    }
}
