package ru.kislyak.projects.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kislyak.projects.dao.FileDAO;
import ru.kislyak.projects.pojos.File;
import ru.kislyak.projects.pojos.Project;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService implements Serializable {
    private FileDAO fileDAO;

    @Autowired
    public void setFileDAO(FileDAO fileDAO) {
        this.fileDAO = fileDAO;
    }

    public void save(File file) {
        if (file==null || file.getProject() == null) {
            return;
        }
        fileDAO.save(file);
    }

    public void delete(File file) {
        if (file == null) {
            return;
        }
        file.setProject(null);
        fileDAO.delete(file);
    }

    public List<File> getFilesByProject(Project project) {
        if (project==null) {
            return new ArrayList<>();
        }
        return fileDAO.getFilesByProject(project);
    }

    public File getFilesEntityById(Integer id) {
        if (id == null) {
            return null;
        }
        return fileDAO.getById(id);
    }

    public void update(File tempFile) {
        if (tempFile == null) {
            return;
        }
        fileDAO.update(tempFile);
    }
}
