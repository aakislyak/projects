package ru.kislyak.projects.service;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.User;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class RegisterService {
    private UserService userService;
    private EmailService emailService;
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    /**
     * Метод Размещает в БД запрос на регистрацию
     * и оповещает об этом админа
     */
    @Transactional
    public String sendRegister(User newUser){
        newUser.setRolename(userService.getRoleByRolename("user"));
        newUser.setPassword(User.getHash(newUser.getPassword()));       // меняем пароль на его ХЭШ
        userService.save(newUser);                                          //Сохраняем в БД
        String uuid = UUID.randomUUID().toString();                         // Генерим UUID для активации учетной записи
        userService.saveUuidForUser(uuid, newUser.getName());               //Сохраняем UUID в БД
        this.logger.info("New \"user register request\" saved into DB");
        emailService.notifyAdminByEmail(newUser, getLink(uuid));
        return "login";
    }

    String getLink(String uuid){
        HttpServletRequest request = Faces.getRequest();
        String link;
        link="https://";
        link+=request.getLocalName();          //ex 079-5376.kvc.ecp
        link+=":"+request.getLocalPort();      //ex :8080
        link+= request.getContextPath();       //ex /Projects
        link+= "/admin/AcceptNewUser.xhtml";
        link+= "?uuid=";
        link+=  uuid;
        return link;
    }
}
