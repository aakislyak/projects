package ru.kislyak.projects.service;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;
import ru.kislyak.projects.pojos.User;


import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class EmailService {
    private JavaMailSender mailSender;
    private SimpleMailMessage templateMessage;
    private VelocityEngine velocityEngine;
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }
    @Autowired
    public void setTemplateMessage(SimpleMailMessage templateMessage) {
        this.templateMessage = templateMessage;
    }
    @Autowired
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Async
    void notifyAdminByEmail(final User user, final String uuidLink){
        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
            message.setTo(templateMessage.getTo()[0]);
            message.setFrom(templateMessage.getFrom());
            message.setSubject(templateMessage.getSubject());
            Map model = new HashMap();
            model.put("user", user);
            model.put("uuidLink", uuidLink);
            String text = VelocityEngineUtils.mergeTemplateIntoString(
                    velocityEngine, "registration-confirmation.vm", "UTF-8", model);
            message.setText(text, true);
        };
        try {
            this.mailSender.send(preparator);
            this.logger.info("Admin email notification has been sended");
        }
        catch (MailException ex) {
            ex.printStackTrace();
            this.logger.severe("Error during send mail notify to Admin");
            System.err.println(ex.getMessage());
        }
    }
}
