package ru.kislyak.projects.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kislyak.projects.dao.ProjectDAO;
import ru.kislyak.projects.pojos.Designer;
import ru.kislyak.projects.pojos.Project;

import java.util.List;

@Service
public class ProjectService {
    private ProjectDAO projectDAO;
    private DesignerService designerService;

    @Autowired
    public void setDesignerService(DesignerService designerService) {
        this.designerService = designerService;
    }

    @Autowired
    public void setProjectDAO(ProjectDAO projectDAO) {
        this.projectDAO = projectDAO;
    }

    /**
     * устанавливает значение поля проектировщик
     * добавляемому проекту
     */
    Project assignProjectDesigner(Project project, int selectedDesigner) {
        for (Designer d : designerService.getDesigners()) {
            if (d.getId() == selectedDesigner) {
                project.setDesigner(d);
                return project;
            }
        }
        return project;
    }

    public void addProject(Project project, int selectedDesigner) {
        if (project == null) {
            return;
        }
        project = assignProjectDesigner(project, selectedDesigner);
        projectDAO.save(project);
    }


    public void deleteProject(Project project) {
        if (project != null) {
            projectDAO.delete(project);
        }
    }

    public void updateProject(Project project, int selectedDesigner) {
        if (project == null){
            return;
        }
        project = assignProjectDesigner(project, selectedDesigner);
        projectDAO.update(project);
    }

    public List<Project> getAllProjects() {
        return projectDAO.getAll();
    }

    public List<Designer> getAllDesigners() {
        return designerService.getDesigners();
    }
}
