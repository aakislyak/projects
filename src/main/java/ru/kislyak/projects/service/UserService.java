package ru.kislyak.projects.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kislyak.projects.dao.RoleDAO;
import ru.kislyak.projects.dao.UserDAO;
import ru.kislyak.projects.pojos.Role;
import ru.kislyak.projects.pojos.User;
import ru.kislyak.projects.pojos.Uuid;

import java.io.Serializable;
import java.util.List;


@Service
public class UserService implements Serializable {
    private UserDAO userDAO;
    private RoleDAO roleDAO;
    private UuidService uuidService;

    @Autowired
    public void setRoleDAO(RoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }

    @Autowired
    public void setUuidService(UuidService uuidService) {
        this.uuidService = uuidService;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public Uuid findUuid(String uuid) {
        return uuidService.getByUuid(uuid);
    }

    public void deleteUser(User user) {
        if (user != null) {
            userDAO.delete(user);
        }
    }

    //  Админский метод для изменения роли пользователя
    public void updateUserByAdmin(User user, int selectedRole) {
        if (user == null) {
            return;
        }
        user = assignUserRole(user, selectedRole);
        userDAO.update(user);
    }

    //  метод для обновления пользовательских данных
    public void updateUser(User user) {
        if (user == null) {
            return;
        }
        user.setPassword(User.getHash(user.getPassword())); //  меняем пароль на Хэш
        userDAO.update(user);
    }

    //  Метод устанавливает значение поля роль
    // текущему экземпляру бина "UserEntity"
    User assignUserRole(User user, int selectedRole) {
        for (Role role : getAllRoles()) {
            if (role.getId() == selectedRole) {
                user.setRolename(role);
                break;
            }
        }
        return user;
    }

    // метод возвращает все роли из БД
    public List<Role> getAllRoles() {
        return roleDAO.getAll();
    }

    //метод возвращает всех пользователей из БД
    public List<User> getUsersList() {
        return userDAO.getAll();
    }

    // метод предназначен для регистрации новых пользователей в приложении
    public void save(User newUser) {
        if (newUser == null) {
            return;
        }
        userDAO.save(newUser);
    }

    public void deleteUuidByUsername(String username) {
        uuidService.deleteUuidByUsername(username);
    }

    //метод сохраняет уникальный идентификатор в БД и связывает с ним аккаунт
    void saveUuidForUser(String uuid, String username) {
        if (username == null || uuid == null) {
            return;
        }
        Uuid uuidEntity = new Uuid();
        uuidEntity.setUuid(uuid);
        uuidEntity.setUser(userDAO.getByUsername(username));
        uuidService.saveUuid(uuidEntity);
    }

    public User getUserEntityByName(String username) {
        if (username == null) {
            return null;
        }
        return userDAO.getByUsername(username);
    }

    Role getRoleByRolename(String roleName) {
        return roleDAO.getRoleByRolename(roleName);
    }
}
