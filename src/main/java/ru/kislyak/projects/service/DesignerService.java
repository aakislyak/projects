package ru.kislyak.projects.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kislyak.projects.dao.DesignerDAO;
import ru.kislyak.projects.pojos.Designer;

import java.io.Serializable;
import java.util.List;

@Service
public class DesignerService implements Serializable {

    private DesignerDAO designerDAO;

    public int addDesigner(Designer designer) {
        if (designer != null){
            return this.designerDAO.save(designer);
        }
        return 0;
    }

    public void deleteDesigner(Designer designer) {
        if (designer != null) {
            designerDAO.delete(designer);
        }
    }

    public void updateDesigner(Designer designer) {
        if (designer==null){return;}
        designerDAO.update(designer);
    }

    public List<Designer> getDesigners() {
        return this.designerDAO.getAll();
    }

    @Autowired
    public void setDesignerDAO(DesignerDAO designerDAO) {
        this.designerDAO = designerDAO;
    }
}
