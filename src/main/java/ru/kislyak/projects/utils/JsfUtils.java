package ru.kislyak.projects.utils;

import org.omnifaces.util.Faces;

import javax.faces.application.FacesMessage;

public class JsfUtils {
    public static void addErrorMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ошибка", msg);
        Faces.getContext().addMessage("Ошибка", facesMsg);
    }

    public static void addSuccessMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Успех", msg);
        Faces.getContext().addMessage("Успех", facesMsg);
    }
}
