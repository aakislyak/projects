package ru.kislyak.projects.controler;

import org.springframework.security.core.context.SecurityContextHolder;
import ru.kislyak.projects.pojos.User;
import ru.kislyak.projects.service.UserService;
import ru.kislyak.projects.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

@ManagedBean(name = "loginController")
@SessionScoped
public class LoginController implements Serializable {
    @ManagedProperty("#{userService}")
    private UserService userService;
    private User currentUser;

    @PostConstruct
    private void init(){
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        this.currentUser = userService.getUserEntityByName(userName);    //  Инициализируем значение поля, содержащего всю инфу о текущем пользователе
    }

    public void updateUser() {
        this.userService.updateUser(currentUser);
        JsfUtils.addSuccessMessage("Данные сохранены"); // Показываем сообщение об успехе
}

    /**
     * метод возвращает шаблон для JSF страницы
     * в зависимости от роли пользователя
     */
    public String getTemplate() {
        String role = this.currentUser.getRolename().getRolename();
        return "/" + role + "/" + role.substring(0, 1).toUpperCase() + role.substring(1) + "Template.xhtml";
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
