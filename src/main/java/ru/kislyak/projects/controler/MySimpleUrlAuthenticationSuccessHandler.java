package ru.kislyak.projects.controler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.omnifaces.util.Faces;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.SavedRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;

public class MySimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private Log logger = LogFactory.getLog(this.getClass());
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication) throws IOException {

        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }

    private void handle(HttpServletRequest request,
                          HttpServletResponse response, Authentication authentication) throws IOException {

        String targetUrl = determineTargetUrl(authentication);
        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    /** Builds the target URL according to the logic defined in the main class Javadoc. */
    private String determineTargetUrl(Authentication authentication) {
        SavedRequest savedRequest = (SavedRequest)Faces.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        String requestedUrl="";
        if (savedRequest!=null){
            requestedUrl = savedRequest.getRedirectUrl();
        }

        boolean isUser = false;
        boolean isAdmin = false;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            logger.warn("Application \"Projects\" try to loggin by name: " + authentication.getName());
            if (grantedAuthority.getAuthority().equals("ROLE_user")) {
                isUser = true;
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_admin")) {
                isAdmin = true;
                break;
            }
        }

        if (isUser) {
            return "/user/UserTemplate.xhtml";
        }else if (isAdmin && isAcceptRequest(requestedUrl)){
            assert savedRequest != null;
            String[] uuidParameterValues = savedRequest.getParameterValues("uuid");
            return "/admin/AcceptNewUser.xhtml"+"?uuid="+uuidParameterValues[0];
        }else if (isAdmin) {
            return "/admin/AdminTemplate.xhtml";
        } else {
            throw new IllegalStateException();
        }
    }

    private boolean isAcceptRequest (String requestedUrl){
        return requestedUrl.contains("uuid");
    }

    private void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}
