package ru.kislyak.projects.controler;

import ru.kislyak.projects.pojos.Designer;
import ru.kislyak.projects.pojos.Project;
import ru.kislyak.projects.service.ProjectService;
import ru.kislyak.projects.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

@ManagedBean(name = "projectController")
@ViewScoped
public class ProjectController implements Serializable{
    @ManagedProperty("#{projectService}")
    private ProjectService projectService;

    private List<Project> projectsList;
    private Project tempProject;
    private int selectedDesigner;
    private List<Designer> allDesigners;

    // инициализируем список проектов и список проектировщиков
    //  Запрашиваем полный перечень проектировщиков из БД
    @PostConstruct
    private void init(){
        this.allDesigners=projectService.getAllDesigners();
        this.projectsList=projectService.getAllProjects();
        this.tempProject = null;
    }

    public List<Project> getProjectsList() {
        return this.projectsList;
    }

    // Инициализируем временное поле где будем хранить редактируемый/добавляемы проект
    public Project prepareAddProject() {
        tempProject = new Project();
        return tempProject;
    }

    // обновляем данные о проекте в БД
    public void update() {
        projectService.updateProject(tempProject, selectedDesigner);
        JsfUtils.addSuccessMessage("Проект изменен!");
        init();     //Освежаем список проектов
    }

    // устанавливаем положение селектбокса на текущего проектировщика
    public void prepareEditProject() {
        if (tempProject.getDesigner() != null) {
            this.selectedDesigner = tempProject.getDesigner().getId();
        } else this.selectedDesigner = 0;
    }

    // удаляем выбранный проект
    public void deleteProject() {
        projectService.deleteProject(tempProject);
        JsfUtils.addSuccessMessage("Проект удален!");
        init();     //Освежаем список проектов
    }

    // добавляем проект
    public void addProject() {
        projectService.addProject(tempProject,selectedDesigner);
        JsfUtils.addSuccessMessage("Проект добавлен!");
        init();     //Освежаем список проектов
    }

    //Запрашиваем фамилию текущего проектировщика
    public String getCurentDesignerLastname(){
        if (tempProject.getDesigner() != null) {
            int currId = tempProject.getDesigner().getId();
            for (Designer designer: allDesigners) {
                if (designer.getId()==currId){
                    return designer.getLastname();
                }
            }
        }
        return null;
    }

    public boolean filterByStatus(Object value, Object filter, Locale locale){
        return value.toString().toLowerCase().startsWith(filter.toString().toLowerCase());
    }

    public List<Designer> getAllDesigners() { return this.allDesigners;    }

    public void setAllDesigners(List<Designer> allDesigners) {
        this.allDesigners = allDesigners;
    }

    public Integer getSelectedDesigner() {
        return selectedDesigner;
    }

    public void setSelectedDesigner(Integer selectedDesigner) {
        this.selectedDesigner = selectedDesigner;
    }

    public Project getTempProject() {
        return tempProject;
    }

    public void setTempProject(Project tempProject) {
        this.tempProject = tempProject;
    }

    public void setProjectsList(List<Project> projectsList) {
        this.projectsList = projectsList;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }
}
