package ru.kislyak.projects.controler;

import org.omnifaces.util.Faces;
import ru.kislyak.projects.pojos.User;
import ru.kislyak.projects.service.UserService;
import ru.kislyak.projects.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.logging.Logger;

@ManagedBean(name = "acceptController")
@ViewScoped
public class AcceptController implements Serializable {
    private final String adminTemplate = "/admin/AdminTemplate.xhtml";

    @ManagedProperty("#{userService}")
    private UserService userService;
    private final Logger logger = Logger.getLogger(getClass().getName());
    private User tempUser;
    private int selectedRole;

    private String uuid;

    @PostConstruct
    public void init() {
        try {
            uuid = String.valueOf(Faces.getExternalContext().getRequestParameterMap().get("uuid"));
            tempUser = userService.findUuid(uuid).getUser();
            if (tempUser.getRolename() != null) {
                this.selectedRole = tempUser.getRolename().getId();
            } else this.selectedRole = 0;

        } catch (Exception e) {
            this.logger.severe("Some error heppens due fetch UserEntity from DB with UUID: " + uuid);
            System.err.println(e.getMessage());
        }
    }

    public String deleteUser() {
        userService.deleteUuidByUsername(tempUser.getName());
        userService.deleteUser(tempUser);
        JsfUtils.addSuccessMessage("Регистрация не подтверждена, пользователь удален!");
        return adminTemplate;
    }

    public String acceptUser() {
        tempUser.setAccepted(true);
        userService.updateUserByAdmin(tempUser, selectedRole);
        userService.deleteUuidByUsername(tempUser.getName());
        JsfUtils.addSuccessMessage("Регистрация подтверждена");
        return adminTemplate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTempUser(User tempUser) {
        this.tempUser = tempUser;
    }

    public User getTempUser() {
        return tempUser;
    }

    public int getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(int selectedRole) {
        this.selectedRole = selectedRole;
    }
}

