package ru.kislyak.projects.controler;

import ru.kislyak.projects.pojos.Designer;
import ru.kislyak.projects.service.DesignerService;
import ru.kislyak.projects.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "designerController")
@ViewScoped
public class DesignerController  implements Serializable {
    @ManagedProperty("#{designerService}")
    private DesignerService designerService;
    private List<Designer> designers;
    private Designer selectedDesigner;

    @PostConstruct
    private void init(){
        selectedDesigner = null;
        this.designers=designerService.getDesigners();
    }

    public void addDesigner() {
        designerService.addDesigner(selectedDesigner);
        JsfUtils.addSuccessMessage("Проектировщик добавлен");
        init();
    }

    public void deleteDesigner() {
        designerService.deleteDesigner(selectedDesigner);
        JsfUtils.addSuccessMessage("Проектировщик удален");
        init();
    }

    public void updateDesigner() {
        designerService.updateDesigner(selectedDesigner);
        JsfUtils.addSuccessMessage("Проектировщик изменен");

        init();
    }

    public List<Designer> getDesigners() {
        return this.designers;
    }

    public Designer getSelectedDesigner() {
        return selectedDesigner;
    }

    public void setSelectedDesigner(Designer selectedDesigner) {
        this.selectedDesigner = selectedDesigner;
    }

    //  инициализируем поле, где будем хранить временно создаваемую сущность
    public void prepareAddDesigner() {
        selectedDesigner = new Designer();
    }

    public void setDesignerService(DesignerService designerService) {
        this.designerService = designerService;
    }

    public void setDesigners(List<Designer> designers) {
        this.designers = designers;
    }
}
