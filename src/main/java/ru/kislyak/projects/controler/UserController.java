package ru.kislyak.projects.controler;

import ru.kislyak.projects.pojos.Role;
import ru.kislyak.projects.pojos.User;
import ru.kislyak.projects.service.UserService;
import ru.kislyak.projects.utils.JsfUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "userController")
@ViewScoped
public class UserController implements Serializable {
    @ManagedProperty("#{userService}")
    private UserService userService;

    private User selectedUser;
    private Role selectedRole;
    private List<User> usersList;

    @PostConstruct
    private void init() {
        this.usersList = userService.getUsersList();
    }

    //  метод удаляет запись из БД
    public void deleteUser() {
        userService.deleteUser(selectedUser);
        selectedUser = null;
        JsfUtils.addSuccessMessage("Пользователь удален!");
        init();
    }

    // метод обновляет данные пользователя
    public void updateUser() {
        userService.updateUserByAdmin(selectedUser, selectedRole.getId());
        JsfUtils.addSuccessMessage("Пользователь изменен!");
        selectedUser = null;
        selectedRole = null;
        init();
    }

    //  устанавливаем указатель менюшки на текущую роль
    public void prepareEditUser() {
        if (selectedUser.getRolename() != null) {
            this.selectedRole = selectedUser.getRolename();
        } else this.selectedRole = null;
    }

    //Запрашиваем список ролей для заполнения
    //выпадающего меню
    public List<Role> getAllRoles() {
        return userService.getAllRoles();
    }

    // Запрашиваем список пользователей из БД
    public List<User> getUsersList() {
        return this.usersList;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public Role getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Role selectedRole) {
        this.selectedRole = selectedRole;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
