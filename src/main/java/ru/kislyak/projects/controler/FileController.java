package ru.kislyak.projects.controler;

import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import ru.kislyak.projects.pojos.FilesBinData;
import ru.kislyak.projects.pojos.File;
import ru.kislyak.projects.service.FileService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@ManagedBean(name = "fileController")
@ViewScoped
public class FileController implements Serializable {
    @ManagedProperty("#{fileService}")
    private FileService fileService;
    @ManagedProperty("#{projectController}")
    private ProjectController projectController;

    private List<File> fileList;
    private File selectedFile;

    private void initializeFileList(){
        fileList =fileService.getFilesByProject(projectController.getTempProject());
    }

    /**
     * Метод производит скачку выбранного файла
     */
    public void downloadSelectedFile(){
        selectedFile = fileService.getFilesEntityById(selectedFile.getId());    // инициализируем бинарные данные Lazy
        byte[] FileInBytes= selectedFile.getContents().getBinData();
        String fileName= selectedFile.getFname();
        try {
            Faces.sendFile(FileInBytes, fileName, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void downloadAllFiles(){
        try {
            Faces.sendFile(zipBytes(), projectController.getTempProject().getInvnum()+".zip", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] zipBytes () {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);

        try{
            for(File file : fileList){
                file=fileService.getFilesEntityById(file.getId());
                if(file.getContents() != null){
                    ZipEntry entry = new ZipEntry(file.getFname());
                    zos.putNextEntry(entry);
                    byte[] fileBinData = file.getContents().getBinData();
                    zos.write(fileBinData, 0, fileBinData.length);
                    zos.closeEntry();
                }
            }
            zos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

    public void addFileToAttachment(FileUploadEvent event) {
        List<UploadedFile> uploadAttachment = new LinkedList<>();
        uploadAttachment.add(event.getFile());
        // Проходим по списку и заполняем список файлов сущностей
        for (UploadedFile file: uploadAttachment) {
            File tmpFileEntity = new File();
            tmpFileEntity.setFname(file.getFileName());
            tmpFileEntity.setFsize(file.getSize());
            tmpFileEntity.setContenttype(file.getContentType());
            tmpFileEntity.setProject(projectController.getTempProject());    //Берем из соседнего бина
            FilesBinData binEntity = new FilesBinData();
            binEntity.setBinData(file.getContents());
            tmpFileEntity.setContents(binEntity);
            fileService.save(tmpFileEntity);    // Сохраняем в БД
        }
        initializeFileList();
    }

    //  Удаляем выбранный файл
    public void deleteFile(){
        fileService.delete(selectedFile);
        selectedFile =null;
        initializeFileList();
    }

    public List<File> getFileList() {
        return this.fileList;
    }

    public void update(){
        fileService.update(selectedFile);
        selectedFile =null;
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
    }

    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    public void setProjectController(ProjectController projectController) {
        this.projectController = projectController;
    }

    public File getSelectedFile() {
        return selectedFile;
    }

    public void setSelectedFile(File selectedFile) {
        this.selectedFile = selectedFile;
    }
}
