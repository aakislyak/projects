package ru.kislyak.projects.controler;

import ru.kislyak.projects.pojos.User;
import ru.kislyak.projects.service.RegisterService;
import ru.kislyak.projects.service.UserService;
import ru.kislyak.projects.utils.JsfUtils;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.logging.Logger;

@ManagedBean(name = "registerController")
@RequestScoped
public class RegisterController implements Serializable {
    private final Logger logger = Logger.getLogger(getClass().getName());
    @ManagedProperty("#{registerService}")
    private RegisterService registerService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    public String sendRegister(User newUser) {
        this.logger.info("Usser: " + newUser.toString() + "send request to register in Application \"Projects\"");
        JsfUtils.addSuccessMessage("Администратор приложения должен подтвердить регистрацию!");
        return registerService.sendRegister(newUser);
    }

    /**
     * Метод проверяет существование такого пользователя в системе
     * если находит в БД пользоывателя, то выдает ValidatorException
     */
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String username = (String) value;
        User user = userService.getUserEntityByName(username);
        if (user != null) {
            String message = MessageFormat.format("{0} - такой пользователь уже существует!", username);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ошибка", message);
            throw new ValidatorException(facesMessage);
        }
    }

    public void setRegisterService(RegisterService registerService) {
        this.registerService = registerService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
