package ru.kislyak.projects.dao;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kislyak.projects.pojos.Designer;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testApplicationContext.xml")
@Transactional
public class DesignerDAOImplTest {

    @Autowired
    private DesignerDAO designerDAO;
    @Autowired
    private SessionFactory sessionFactory;

    @After
    public void eraseDesignerTable(){
        List<Designer> designers = designerDAO.getAll();
        designers.forEach(d->designerDAO.delete(d));
        sessionFactory.getCurrentSession().flush();
    }

    @Test
    public void InsertDesignerThenFetchByIdPassTest() throws Exception {
        Designer insertedDesigner = new Designer();
        insertedDesigner.setLastname("Фамилия");
        designerDAO.save(insertedDesigner);
        sessionFactory.getCurrentSession().flush();

        Query q = sessionFactory.getCurrentSession().createQuery("select d from Designer d where d.id = :idVal");
        q.setParameter(String.valueOf("idVal"), insertedDesigner.getId());
        Designer selectedDEsigner = (Designer) q.getSingleResult();
        assertEquals(insertedDesigner.getLastname(), selectedDEsigner.getLastname());
    }

    @Test
    public void InsertNullDesignerFailOrPassTest() throws Exception {
        try {
            designerDAO.save(null);
            fail();
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    public void InsertDesignerWithNullLastnamePassTest() throws Exception {
        Designer insertedDesigner = new Designer();
        designerDAO.save(insertedDesigner);
        sessionFactory.getCurrentSession().flush();

        Query q = sessionFactory.getCurrentSession().createQuery("select d from Designer d where d.id = :idVal");
        q.setParameter(String.valueOf("idVal"), insertedDesigner.getId());
        Designer selectedDEsigner = (Designer) q.getSingleResult();
        assertNull(selectedDEsigner.getLastname());
    }

    @Test
    public void deleteDesignerByEntityTest() {
        Designer insertedDesigner = new Designer();
        insertedDesigner.setLastname("Фамилия");
        designerDAO.save(insertedDesigner);
        sessionFactory.getCurrentSession().flush();

        designerDAO.delete(insertedDesigner);
        sessionFactory.getCurrentSession().flush();

        Designer deletedDesigner = sessionFactory.getCurrentSession().get(Designer.class, insertedDesigner.getId());
        assertNull(deletedDesigner);
    }

    @Test
    public void deleteNullDesignerThenCatchExceptionTest() {
        try {
            designerDAO.delete(null);
            fail();
        } catch (Exception e) {
            assertNotNull(e);
        }
    }


    @Test
    public void getDesignerByIdTest() throws Exception {
        Designer insertedDesigner = new Designer();
        insertedDesigner.setLastname("Фамилия");
        designerDAO.save(insertedDesigner);
        sessionFactory.getCurrentSession().flush();

        Designer selectedDesigner = designerDAO.getById(insertedDesigner.getId());
        assertEquals(insertedDesigner.getId(), selectedDesigner.getId());
    }

    @Test
    public void getDesignerByNullId() {
        try {
            assertNull(designerDAO.getById(null));
            fail();
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    public void InsertThreeDesignersThenGetAllTest(){
        Designer insertedDesigner1 = new Designer();
        Designer insertedDesigner2 = new Designer();
        Designer insertedDesigner3 = new Designer();
        insertedDesigner1.setLastname("Фамилия1");
        insertedDesigner2.setLastname("Фамилия1");
        insertedDesigner3.setLastname("Фамилия1");
        designerDAO.save(insertedDesigner1);
        designerDAO.save(insertedDesigner2);
        designerDAO.save(insertedDesigner3);
        sessionFactory.getCurrentSession().flush();

        List<Designer> designers = designerDAO.getAll();
        assertEquals(3, designers.size());
    }

    @Test
    public void insertDesignerThenUpdateTest(){
        Designer insertedDesigner = new Designer();
        insertedDesigner.setLastname("Фамилия");
        designerDAO.save(insertedDesigner);
        sessionFactory.getCurrentSession().flush();
        insertedDesigner.setLastname("Фамилия1");
        designerDAO.update(insertedDesigner);
        sessionFactory.getCurrentSession().flush();

        Designer selectedDesigner = sessionFactory.getCurrentSession().get(Designer.class, insertedDesigner.getId());
        assertEquals(selectedDesigner.getLastname(), "Фамилия1");
    }

    @Test
    public void tryUpdateNullDesignerMustFailTest(){
        try {
            designerDAO.update(null);
            fail();
        } catch (Exception e){
            assertNotNull(e);
        }
    }

    @Test
    public void deleteDesignerByIdTest(){
        Designer insertedDesigner = new Designer();
        insertedDesigner.setLastname("Фамилия");
        designerDAO.save(insertedDesigner);
        sessionFactory.getCurrentSession().flush();
        designerDAO.deleteById(insertedDesigner.getId());
        sessionFactory.getCurrentSession().flush();

        Designer selectedDesigner = sessionFactory.getCurrentSession().get(Designer.class, insertedDesigner.getId());
        assertNull(selectedDesigner);
    }

    @Test
    public void tryDeleteDesignerByNullId(){
        try {
            designerDAO.deleteById(null);
            fail();
        }catch (Exception e){
            assertNotNull(e);
        }
    }
}