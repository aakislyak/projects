package ru.kislyak.projects.dao;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.kislyak.projects.pojos.Role;


import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testApplicationContext.xml")
@Transactional
public class RoleDAOImplTest {
    @Autowired
    private RoleDAO roleDAO;
    @Autowired
    private SessionFactory sessionFactory;

    @Before
    public void setUp() throws Exception {
        Role currentRole = new Role();
        currentRole.setRolename("admin");
        currentRole.setId((Integer) sessionFactory.getCurrentSession().save(currentRole));
    }

    @After
    public void tearDown() throws Exception {
        Query q = sessionFactory.getCurrentSession().createQuery("delete from Role");
        q.executeUpdate();
    }


    @Test
    public void getAll() throws Exception {
        Role role1 = new Role();
        Role role2 = new Role();
        Role role3 = new Role();
        role1.setRolename("role1");
        role2.setRolename("role2");
        role3.setRolename("role3");
        sessionFactory.getCurrentSession().save(role1);
        sessionFactory.getCurrentSession().save(role2);
        sessionFactory.getCurrentSession().save(role3);
        sessionFactory.getCurrentSession().flush();

        List<Role> roles = roleDAO.getAll();
        assertTrue(roles.size()==4);    //С учетом @Before
    }

    @Test
    public void getRoleByRolename() throws Exception {
        Role selectedRole = roleDAO.getRoleByRolename("admin");
        assertEquals("admin", selectedRole.getRolename());
    }

}