package ru.kislyak.projects.dao;

import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kislyak.projects.pojos.FilesBinData;
import ru.kislyak.projects.pojos.File;
import ru.kislyak.projects.pojos.Project;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testApplicationContext.xml")
//@Commit
@Transactional
public class FileDAOImplTest {
    private Project currentProject;

    private FilesBinData currentBinData;
    private File currentFile;
    @Autowired
    private FileDAO fileDAO;

    @Autowired
    private SessionFactory sessionFactory;

    @Before
    public void setUp() throws Exception {
        System.out.println("*****Start before");
        currentProject = new Project();
        currentProject.setId((Integer) sessionFactory.getCurrentSession().save(currentProject));

        currentBinData = new FilesBinData();
        currentBinData.setBinData(new byte[]{(byte) 0x12, (byte) 0x13});
        currentBinData.setId((Integer) sessionFactory.getCurrentSession().save(currentBinData));

        currentFile = new File();
        currentFile.setContenttype("textTest");
        currentFile.setFname("имя файлаTest");
        currentFile.setNote("ПримечаниеTest");
        currentFile.setFsize(500L);
        System.out.println("*****End before");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("*****Start After");
        sessionFactory.getCurrentSession().delete(currentProject);
        Query q = sessionFactory.getCurrentSession().createQuery("select f from  File f");
        List<File> files = q.getResultList();
        if (!files.isEmpty()) {
            files.forEach(f -> sessionFactory.getCurrentSession().delete(f));
        }
        currentProject = null;
        currentBinData = null;
        currentFile = null;
        System.out.println("*****End After");
    }

    @Test
    public void getFileByIdTest() throws Exception {
        currentFile.setProject(sessionFactory.getCurrentSession().get(Project.class, currentProject.getId()));
        currentFile.setContents(sessionFactory.getCurrentSession().get(FilesBinData.class, currentBinData.getId()));
        sessionFactory.getCurrentSession().save(currentFile);
        sessionFactory.getCurrentSession().flush();

        File selectedFile = fileDAO.getById(currentFile.getId());
        assertTrue(selectedFile.getFsize() == 500L);
        assertEquals("textTest", selectedFile.getContenttype());
        assertEquals("имя файлаTest", selectedFile.getFname());
        assertEquals("ПримечаниеTest", selectedFile.getNote());
    }

    @Test
    public void getFileByNullIdTest() {
        try {
            fileDAO.getById(null);
            fail();
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    public void insertFileThenFetchTest() throws Exception {
        currentFile.setProject(sessionFactory.getCurrentSession().get(Project.class, currentProject.getId()));
        currentFile.setContents(sessionFactory.getCurrentSession().get(FilesBinData.class, currentBinData.getId()));

        fileDAO.save(currentFile);
        sessionFactory.getCurrentSession().flush();

        File selectedFile = sessionFactory.getCurrentSession().get(File.class, currentFile.getId());
        assertEquals(Arrays.toString(new byte[]{(byte) 0x12, (byte) 0x13}), Arrays.toString(selectedFile.getContents().getBinData()));
        assertTrue(selectedFile.getFsize() == 500L);
        assertEquals("textTest", selectedFile.getContenttype());
        assertEquals("имя файлаTest", selectedFile.getFname());
        assertEquals("ПримечаниеTest", selectedFile.getNote());
    }

    @Test
    public void deleteFileThenFetchTest() throws Exception {
        currentFile.setProject(sessionFactory.getCurrentSession().get(Project.class, currentProject.getId()));
        currentFile.setContents(sessionFactory.getCurrentSession().get(FilesBinData.class, currentBinData.getId()));
        sessionFactory.getCurrentSession().save(currentFile);
        sessionFactory.getCurrentSession().flush();

        fileDAO.delete(currentFile);
        sessionFactory.getCurrentSession().flush();

        File selectedFile = sessionFactory.getCurrentSession().get(File.class, currentFile.getId());
        assertNull(selectedFile);
    }

    @Test
    public void getFilesByProjectTest() throws Exception {
        currentFile.setProject(sessionFactory.getCurrentSession().get(Project.class, currentProject.getId()));
        currentFile.setContents(sessionFactory.getCurrentSession().get(FilesBinData.class, currentBinData.getId()));
        sessionFactory.getCurrentSession().save(currentFile);
        sessionFactory.getCurrentSession().flush();

        List<File> selectedFiles = fileDAO.getFilesByProject(currentProject);
        assertTrue(selectedFiles.size() > 0);
    }

    @Test
    public void updateFileTest() throws Exception {
        currentFile.setProject(sessionFactory.getCurrentSession().get(Project.class, currentProject.getId()));
        currentFile.setContents(sessionFactory.getCurrentSession().get(FilesBinData.class, currentBinData.getId()));
        sessionFactory.getCurrentSession().save(currentFile);
        sessionFactory.getCurrentSession().flush();

        currentFile.setFsize(600L);
        currentFile.setNote("updateTestNote");
        currentFile.setContenttype("updateTestContentsType");
        currentFile.setFname("updateTestFname");
        fileDAO.update(currentFile);
        sessionFactory.getCurrentSession().flush();

        File selectedFile = sessionFactory.getCurrentSession().get(File.class, currentFile.getId());
        assertTrue(selectedFile.getFsize() == 600L);
        assertEquals("updateTestNote", selectedFile.getNote());
        assertEquals("updateTestContentsType", selectedFile.getContenttype());
        assertEquals("updateTestFname", selectedFile.getFname());
    }
}