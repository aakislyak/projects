package ru.kislyak.projects.dao;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kislyak.projects.pojos.Designer;
import ru.kislyak.projects.pojos.Project;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testApplicationContext.xml")
@Transactional
public class ProjectDAOImplTest {
    @Autowired
    private ProjectDAO projectDAO;
    @Autowired
    private SessionFactory sessionFactory;
    private Project currentProject;
    private Integer         currentProjectId;
    private Designer currentDesigner;

    @Before
    public void init() {
        currentDesigner = new Designer();
        currentDesigner.setLastname("Lastname");
        sessionFactory.getCurrentSession().save(currentDesigner);

        currentProject = new Project();
        currentProject.setNote("project note");
        currentProject.setInvnum("ASUZH");
        currentProject.setLibnum("45/16");
        currentProject.setObjects("building xxx");
        currentProject.setTitle("project title");
        currentProject.setCustomer("project customer");
        currentProject.setRealised(true);
        currentProject.setYear("2015");
        currentProject.setDesigner(currentDesigner);
        currentProjectId = (Integer) sessionFactory.getCurrentSession().save(currentProject);
    }

    @Test
    public void getByIdTest() throws Exception {
        Project selectedProject = projectDAO.getById(currentProjectId);
        assertEquals("project note", selectedProject.getNote());
        assertEquals("ASUZH", selectedProject.getInvnum());
        assertEquals("45/16", selectedProject.getLibnum());
        assertEquals("building xxx", selectedProject.getObjects());
        assertEquals("project title", selectedProject.getTitle());
        assertEquals("project customer", selectedProject.getCustomer());
        assertTrue(selectedProject.getRealised());
        assertEquals("2015", selectedProject.getYear());
        assertEquals("Lastname", selectedProject.getDesigner().getLastname());
    }

    @Test (expected = Exception.class)
    public void getByNullIdTest() {
        projectDAO.getById(null);
    }


    @Test
    public void getAllProjectsTest() throws Exception {
        List<Project> selectedProjects = projectDAO.getAll();
        assertTrue(selectedProjects.size()==1);
    }

    @Test
    public void saveProjectTest() throws Exception {
        Project insertedProject = new Project();
        insertedProject = fillProjectFields(insertedProject);
        insertedProject.setDesigner(currentDesigner);
        projectDAO.save(insertedProject);

        Project selectedProject = sessionFactory.getCurrentSession().get(Project.class, insertedProject.getId());
        assertEquals("project note1", selectedProject.getNote());
        assertEquals("ASUZH1", selectedProject.getInvnum());
        assertEquals("45/161", selectedProject.getLibnum());
        assertEquals("building xxx1", selectedProject.getObjects());
        assertEquals("project title1", selectedProject.getTitle());
        assertEquals("project customer1", selectedProject.getCustomer());
        assertFalse(selectedProject.getRealised());
        assertEquals("20151", selectedProject.getYear());

    }

    @Test
    public void updateProjectTest() throws Exception {
        currentProject=fillProjectFields(currentProject);
        projectDAO.update(currentProject);

        Project selectedProject = sessionFactory.getCurrentSession().get(Project.class, currentProject.getId());
        assertEquals("project note1", selectedProject.getNote());
        assertEquals("ASUZH1", selectedProject.getInvnum());
        assertEquals("45/161", selectedProject.getLibnum());
        assertEquals("building xxx1", selectedProject.getObjects());
        assertEquals("project title1", selectedProject.getTitle());
        assertEquals("project customer1", selectedProject.getCustomer());
        assertFalse(selectedProject.getRealised());
        assertEquals("20151", selectedProject.getYear());
    }

    @Test(expected = Exception.class)
    public void updateNullProjectTest() {
        projectDAO.update(null);
    }

    @Test
    public void deleteProjectTest() throws Exception {
        projectDAO.delete(currentProject);
        assertNull(sessionFactory.getCurrentSession().get(Project.class, currentProject.getId()));
    }

    @Test
    public void deleteById() throws Exception {
        projectDAO.deleteById(currentProject.getId());
        assertNull(sessionFactory.getCurrentSession().get(Project.class, currentProject.getId()));
    }

    private Project fillProjectFields(Project project) {
        project.setInvnum("ASUZH1");
        project.setNote("project note1");
        project.setObjects("building xxx1");
        project.setTitle("project title1");
        project.setCustomer("project customer1");
        project.setRealised(false);
        project.setYear("20151");
        project.setLibnum("45/161");
        return project;
    }

}