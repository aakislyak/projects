package ru.kislyak.projects.dao;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kislyak.projects.pojos.Role;
import ru.kislyak.projects.pojos.User;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testApplicationContext.xml")
@Transactional

public class UserDAOImplTest {
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private SessionFactory sessionFactory;

    private User currentUser;
    private Integer currentId;

    @Before
    public void init() {
        Role currentRole = new Role();
        currentRole.setRolename("User");
        sessionFactory.getCurrentSession().save(currentRole);

        currentUser = new User(0, "Login", "Password", "Firstname", "Lastname", "Middlename", currentRole, true);
        currentId = (Integer) sessionFactory.getCurrentSession().save(currentUser);
    }

    @Test
    public void getByIdTest() throws Exception {
        User selectedUser = userDAO.getById(currentId);
        assertEquals("Login", selectedUser.getName());
        assertEquals("Password", selectedUser.getPassword());
        assertEquals("Firstname", selectedUser.getFirstname());
        assertEquals("Lastname", selectedUser.getLastname());
        assertEquals("Middlename", selectedUser.getMiddlename());
        assertEquals("User", selectedUser.getRolename().getRolename());
        assertTrue(selectedUser.getAccepted());
    }

    @Test (expected = Exception.class)
    public void getByNullIdTest() throws Exception {
        userDAO.getById(null);
    }

    @Test
    public void getAllUsersTest() throws Exception {
        List<User> selectedUsers = userDAO.getAll();
        assertEquals(1, selectedUsers.size());
    }

    @Test
    public void getByUsername() throws Exception {
        User selectedUser = userDAO.getByUsername("Login");
        assertEquals("Login", selectedUser.getName());
        assertEquals("Password", selectedUser.getPassword());
        assertEquals("Firstname", selectedUser.getFirstname());
        assertEquals("Lastname", selectedUser.getLastname());
        assertEquals("Middlename", selectedUser.getMiddlename());
        assertEquals("User", selectedUser.getRolename().getRolename());
        assertTrue(selectedUser.getAccepted());
    }

    @Test
    public void saveUserTest() throws Exception {
        Role newRole = new Role();
        newRole.setRolename("Admin");
        sessionFactory.getCurrentSession().save(newRole);
        User newUser = new User(0, "newLogin", "newPassword", "newFirstname", "newLastname", "newMiddlename", newRole, true);
        Integer insertedUserId = userDAO.save(newUser);

        User selectedUser = sessionFactory.getCurrentSession().get(User.class, insertedUserId);
        assertEquals("newLogin", selectedUser.getName());
        assertEquals("newPassword", selectedUser.getPassword());
        assertEquals("newFirstname", selectedUser.getFirstname());
        assertEquals("newLastname", selectedUser.getLastname());
        assertEquals("newMiddlename", selectedUser.getMiddlename());
        assertEquals("Admin", selectedUser.getRolename().getRolename());
        assertTrue(selectedUser.getAccepted());
    }

    @Test (expected = Exception.class)
    public void saveNullUserTest() throws Exception {
        userDAO.save(null);
    }

    @Test
    public void updateUserTest() throws Exception {
        Role newRole = new Role();
        newRole.setRolename("Admin");
        sessionFactory.getCurrentSession().save(newRole);

        currentUser.setName("newLogin");
        currentUser.setPassword("newPassword");
        currentUser.setFirstname("newFirstname");
        currentUser.setLastname("newLastname");
        currentUser.setMiddlename("newMiddlename");
        currentUser.setAccepted(false);
        currentUser.setRolename(newRole);

        userDAO.update(currentUser);

        User selectedUser = sessionFactory.getCurrentSession().get(User.class, currentId);
        assertEquals("newLogin", selectedUser.getName());
        assertEquals("newPassword", selectedUser.getPassword());
        assertEquals("newFirstname", selectedUser.getFirstname());
        assertEquals("newLastname", selectedUser.getLastname());
        assertEquals("newMiddlename", selectedUser.getMiddlename());
        assertEquals("Admin", selectedUser.getRolename().getRolename());
        assertFalse(selectedUser.getAccepted());
    }

    @Test
    public void deleteUserByEntityTest() throws Exception {
        userDAO.delete(currentUser);
        assertNull(sessionFactory.getCurrentSession().get(User.class, currentId));
    }

    @Test
    public void deleteByIdTest() throws Exception {
        userDAO.deleteById(currentId);
        assertNull(sessionFactory.getCurrentSession().get(User.class, currentId));
    }
}