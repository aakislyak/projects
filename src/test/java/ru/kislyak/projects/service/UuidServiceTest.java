package ru.kislyak.projects.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import ru.kislyak.projects.dao.UuidDAO;
import ru.kislyak.projects.pojos.User;
import ru.kislyak.projects.pojos.Uuid;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UuidServiceTest {
    @Mock
    UuidDAO uuidDAOMock;
    @InjectMocks
    UuidService uuidService;

    @Before
    public void setUp() throws Exception {
        when(uuidDAOMock.getByUuid(anyString())).thenAnswer(new Answer<Uuid>() {
            @Override
            public Uuid answer(InvocationOnMock invocationOnMock) throws Throwable {
                Uuid uuid = new Uuid();
                uuid.setUuid((String) invocationOnMock.getArguments()[0]);
                User user = new User();
                user.setName("admin");
                uuid.setUser(user);
                uuid.setId(1);
                return uuid;
            }
        });
    }

    @Test (expected = RuntimeException.class)
    public void saveNotNullUuidTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidDAOMock).save(any(Uuid.class));
        Uuid uuid = new Uuid();
        User user = new User();
        uuid.setUser(user);
        uuidService.saveUuid(uuid);
    }

    @Test
    public void saveNullUuidTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidDAOMock).save(any(Uuid.class));
        uuidService.saveUuid(null);
    }

    @Test
    public void saveNullUserInUuidTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidDAOMock).save(any(Uuid.class));
        Uuid uuid = new Uuid();
        uuidService.saveUuid(uuid);
    }


    @Test
    public void getByUuidTest() throws Exception {
        Uuid uuid = uuidService.getByUuid("02585-522");
        assertEquals("02585-522", uuid.getUuid());
        assertEquals(1, uuid.getId());
        assertEquals("admin", uuid.getUser().getName());
    }

    @Test
    public void getByNullUuidTest() throws Exception {
        Uuid uuid = uuidService.getByUuid(null);
        assertNull(uuid);
    }

    @Test  (expected = RuntimeException.class)
    public void deleteUuidByUsernameTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidDAOMock).deleteUuidByUsername(anyString());
        uuidService.deleteUuidByUsername("admin))");
    }


    @Test
    public void deleteUuidByNullUsernameTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidDAOMock).deleteUuidByUsername(anyString());
        uuidService.deleteUuidByUsername(null);
    }

}