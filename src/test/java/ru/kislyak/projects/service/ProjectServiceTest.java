package ru.kislyak.projects.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import ru.kislyak.projects.dao.ProjectDAO;
import ru.kislyak.projects.pojos.Designer;
import ru.kislyak.projects.pojos.Project;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceTest {
    @Mock
    ProjectDAO projectDAOMock;
    @Mock
    DesignerService designerServiceMock;
    @InjectMocks
    ProjectService projectService;


    @Before
    public void setUp() throws Exception {
        when(designerServiceMock.getDesigners()).thenAnswer(new Answer<List<Designer>>() {
            @Override
            public List<Designer> answer(InvocationOnMock invocationOnMock) throws Throwable {
                List<Designer> designersMock = new ArrayList<>();
                Designer designer1 = new Designer();
                designer1.setId(1);
                designer1.setLastname("testLastname");
                designersMock.add(designer1);
                return designersMock;
            }
        });
    }

    @Test
    public void assignProjectDesignerTest() throws Exception{
        Project insertedProject = new Project();
        Project insertedProjectWithAssignedDesigner = projectService.assignProjectDesigner(insertedProject, 1);
        assertEquals(1, insertedProjectWithAssignedDesigner.getDesigner().getId());
        assertEquals("testLastname", insertedProjectWithAssignedDesigner.getDesigner().getLastname());
    }

    @Test
    public void assignProjectNullDesignerTest() throws Exception{
        Project insertedProject = new Project();
        Project insertedProjectWithAssignedDesigner = projectService.assignProjectDesigner(insertedProject, 0);
        assertNull(insertedProjectWithAssignedDesigner.getDesigner());
    }

    @Test (expected = RuntimeException.class)
    public void addProjectTest() throws Exception {
        Mockito.doThrow(new RuntimeException()).when(projectDAOMock).save(any(Project.class));
        Project insertedProject = new Project();
        projectService.addProject(insertedProject,0);
    }

    @Test
    public void addNullProjectTest() throws Exception {
        Mockito.doThrow(new RuntimeException()).when(projectDAOMock).save(any(Project.class));
        projectService.addProject(null,0);
    }


    @Test (expected = RuntimeException.class)
    public void deleteProjectTest() throws Exception {
        Mockito.doThrow(new RuntimeException()).when(projectDAOMock).delete(any(Project.class));
        Project insertedProject = new Project();
        projectService.deleteProject(insertedProject);
    }


    @Test
    public void deleteNullProjectTest() throws Exception {
        Mockito.doThrow(new RuntimeException()).when(projectDAOMock).delete(any(Project.class));
        projectService.deleteProject(null);
    }

    @Test (expected = RuntimeException.class)
    public void updateProjectTest() throws Exception {
        Mockito.doThrow(new RuntimeException()).when(projectDAOMock).update(any(Project.class));
        Project insertedProject = new Project();
        projectService.updateProject(insertedProject,0);
    }

    @Test
    public void updateNullProjectTest() throws Exception {
        Mockito.doThrow(new RuntimeException()).when(projectDAOMock).update(any(Project.class));
        projectService.updateProject(null,0);
    }

    @Test
    public void getAll() throws Exception {
        when(projectDAOMock.getAll()).thenAnswer(new Answer<List<Project>>() {
            @Override
            public List<Project> answer(InvocationOnMock invocationOnMock) throws Throwable {
                List<Project> projectsMock = new ArrayList<>();
                Project project = new Project();
                projectsMock .add(project);
                return projectsMock;
            }
        });
        assertEquals(1, projectService.getAllProjects().size());
    }

    @Test
    public void getAllDesigners() throws Exception {
        assertEquals(1, projectService.getAllDesigners().size());
    }
}