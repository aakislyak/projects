package ru.kislyak.projects.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import ru.kislyak.projects.pojos.Role;
import ru.kislyak.projects.pojos.User;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RegisterServiceTest {
    @Mock
    UserService userServiceMock;
    @Mock
    EmailService emailServiceMock;
    @InjectMocks
    RegisterService registerService;

    @Before
    public void setUp() throws Exception {
        HttpServletRequest requestMock = mock(HttpServletRequest.class);
        when(requestMock.getLocalName()).thenReturn("079-5376.kvc.ecp");
        when(requestMock.getLocalPort()).thenReturn(8080);
        when(requestMock.getContextPath()).thenReturn("/Projects");

        FacesContext context = ContextMocker.mockFacesContext();
        ExternalContext ext = mock(ExternalContext.class);

        when(context.getExternalContext()).thenReturn(ext);
        when(ext.getRequest()).thenReturn(requestMock);
    }

    @Test
    public void sendRegister() throws Exception {
        when(userServiceMock.getRoleByRolename(anyString())).thenAnswer(new Answer<Role>() {
            @Override
            public Role answer(InvocationOnMock invocationOnMock) throws Throwable {
                Role userRole = new Role();
                userRole.setRolename((String) invocationOnMock.getArguments()[0]);
                userRole.setId(1);
                return userRole;
            }
        });
        doNothing().when(userServiceMock).save(any(User.class));
        doNothing().when(userServiceMock).saveUuidForUser(anyString(),anyString());
        doNothing().when(emailServiceMock).notifyAdminByEmail(any(User.class), anyString());


        User newUser = new User(1, "loginName", "password", "firstname", "lastname", "middlename", null, false);
        String returnedString = registerService.sendRegister(newUser);
        assertEquals("login", returnedString);
    }

    @Test
    public void getLink() throws Exception {

        String uuid = "12345678";
        String registerLink = registerService.getLink(uuid);
        String expectedLink = "https://079-5376.kvc.ecp:8080/Projects/admin/AcceptNewUser.xhtml?uuid="+uuid;
        assertEquals(expectedLink, registerLink);
    }
}