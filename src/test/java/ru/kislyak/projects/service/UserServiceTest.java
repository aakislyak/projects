package ru.kislyak.projects.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import ru.kislyak.projects.dao.RoleDAO;
import ru.kislyak.projects.dao.UserDAO;
import ru.kislyak.projects.pojos.Role;
import ru.kislyak.projects.pojos.User;
import ru.kislyak.projects.pojos.Uuid;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    private static final int ADMIN_ROLE=1;
    private static final int USER_ROLE=2;

    @Mock
    UserDAO userDAOMock;
    @Mock
    RoleDAO roleDAOMock;
    @Mock
    UuidService uuidServiceMock;
    @InjectMocks
    UserService userService;

    @Before
    public void initial() throws Exception {
        when(uuidServiceMock.getByUuid(anyString())).thenAnswer(new Answer<Uuid>() {
            @Override
            public Uuid answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (invocationOnMock.getArguments()[0] == null) {
                    return null;
                }
                Uuid uuid = new Uuid();
                uuid.setUuid((String) invocationOnMock.getArguments()[0]);
                User user = new User();
                user.setName("admin");
                uuid.setUser(user);
                uuid.setId(1);
                return uuid;
            }
        });

        when(roleDAOMock.getAll()).thenAnswer(new Answer<List<Role>>() {
            @Override
            public List<Role> answer(InvocationOnMock invocationOnMock) throws Throwable {
                Role roleAdmin = new Role();
                roleAdmin.setId(ADMIN_ROLE);
                roleAdmin.setRolename("admin");
                Role roleUser = new Role();
                roleUser.setId(USER_ROLE);
                roleUser.setRolename("user");
                List <Role> roles = new ArrayList<>();
                roles.add(roleAdmin);
                roles.add(roleUser);
                return roles;
            }
        });

        when(userDAOMock.getByUsername(anyString())).thenAnswer(new Answer<User>() {
            @Override
            public User answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (invocationOnMock.getArguments()[0] == null) {
                    return null;
                }
                User user = new User();
                user.setId(1);
                user.setName((String) invocationOnMock.getArguments()[0]);
                return user;
            }
        });

        when(roleDAOMock.getRoleByRolename(anyString())).thenAnswer(new Answer<Role>() {
            @Override
            public Role answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (invocationOnMock.getArguments()[0] == null) {
                    return null;
                }
                Role role = new Role();
                role.setRolename((String) invocationOnMock.getArguments()[0]);
                role.setId(1);
                return role;
            }
        });

    }

    @Test
    public void findUuidByUnicIdTest() throws Exception {
        Uuid uuid = userService.findUuid("02585-522");
        assertEquals("02585-522", uuid.getUuid());
        assertEquals(1, uuid.getId());
        assertEquals("admin", uuid.getUser().getName());
    }

    @Test
    public void findUuidByNullUnicIdTest() throws Exception {
        Uuid uuid = userService.findUuid(null);
        assertNull(uuid);
    }

    @Test(expected = RuntimeException.class)
    public void deleteUserTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).delete(any(User.class));
        userService.deleteUser(new User());
    }

    @Test
    public void deleteNullUserTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).delete(any(User.class));
        userService.deleteUser(null);
    }

    @Test (expected = RuntimeException.class)
    public void updateUserByAdminTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).update(any(User.class));
        userService.updateUserByAdmin(new User(),1);
    }

    @Test
    public void updateNullUserByAdminTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).update(any(User.class));
        userService.updateUserByAdmin(null,0);
    }

    @Test  (expected = RuntimeException.class)
    public void updateUserByMySelfTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).update(any(User.class));
        userService.updateUser(new User());
    }

    @Test
    public void updateNullUserByMySelfTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).update(any(User.class));
        userService.updateUser(null);
    }

    @Test
    public void assignUserRoleTest() throws Exception {
        User user = new User(1, "loginName", "password", "firstname", "lastname", "middlename", null, true);
        userService.assignUserRole(user, USER_ROLE);
        User admin = new User(1, "loginName", "password", "firstname", "lastname", "middlename", null, true);
        userService.assignUserRole(admin, ADMIN_ROLE);
        assertEquals(USER_ROLE, user.getRolename().getId());
        assertEquals(ADMIN_ROLE, admin.getRolename().getId());
    }

    @Test
    public void assignUserNullRoleTest() throws Exception {
        User user = new User(1, "loginName", "password", "firstname", "lastname", "middlename", null, true);
        userService.assignUserRole(user, 0);
        assertNull(user.getRolename());
    }

    @Test
    public void getAllRolesTest() throws Exception {
        List<Role> allRoles = userService.getAllRoles();
        assertEquals(2, allRoles.size());
    }

    @Test
    public void getUsersListTest() throws Exception {
        when(userDAOMock.getAll()).thenAnswer(new Answer<List<User>>() {
            @Override
            public List<User> answer(InvocationOnMock invocationOnMock) throws Throwable {
                User admin = new User(1, "loginName", "password", "firstname", "lastname", "middlename", null, true);
                User user = new User(1, "loginName", "password", "firstname", "lastname", "middlename", null, true);
                List<User> users = new ArrayList<>();
                users.add(admin);
                users.add(user);
                return users;
            }
        });
        assertEquals(2, userService.getUsersList().size());
    }

    @Test (expected = RuntimeException.class)
    public void saveUserTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).save(any(User.class));
        userService.save(new User());
    }

    @Test
    public void saveNullUserTest() throws Exception {
        doThrow(new RuntimeException()).when(userDAOMock).save(any(User.class));
        userService.save(null);
    }

    @Test (expected = RuntimeException.class)
    public void deleteUuidByUsernameTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidServiceMock).deleteUuidByUsername(anyString());
        userService.deleteUuidByUsername("testUUID");
        userService.deleteUuidByUsername(null);
    }

    @Test (expected = RuntimeException.class)
    public void saveUuidForUserTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidServiceMock).saveUuid(any(Uuid.class));
        userService.saveUuidForUser("123-123", "admin");
        userService.saveUuidForUser(null, null);
    }

    @Test
    public void saveUuidForNullUserTest() throws Exception {
        doThrow(new RuntimeException()).when(uuidServiceMock).saveUuid(any(Uuid.class));
        userService.saveUuidForUser(null, null);
    }

    @Test
    public void getUserEntityByName() throws Exception {
        User selectedUser = userService.getUserEntityByName("admin");
        assertEquals("admin", selectedUser.getName());
    }

    @Test
    public void getUserEntityByNullName() throws Exception {
        User selectedUser = userService.getUserEntityByName(null);
        assertNull(selectedUser);
    }

    @Test
    public void getRoleByRolenameTest() throws Exception {
        Role selectedRole = userService.getRoleByRolename("admin");
        assertEquals("admin", selectedRole.getRolename());
    }

    @Test
    public void getRoleByNullRolenameTest() throws Exception {
        assertNull(userService.getRoleByRolename(null));
    }

    @Test
    public void getHashTest() throws Exception {
        String hashedPassword = "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4";
        assertEquals(hashedPassword, User.getHash("1234"));
    }
}