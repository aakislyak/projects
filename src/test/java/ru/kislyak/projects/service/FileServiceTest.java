package ru.kislyak.projects.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import ru.kislyak.projects.dao.FileDAO;
import ru.kislyak.projects.pojos.File;
import ru.kislyak.projects.pojos.Project;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceTest {
    @Mock
    FileDAO fileDAOMock;
    @InjectMocks
    FileService fileService;

    @Before
    public void addBehaviour() throws Exception {
        when(fileDAOMock.getFilesByProject(any(Project.class))).thenAnswer(new Answer<List<File>>() {
            @Override
            public List<File> answer(InvocationOnMock invocationOnMock) throws Throwable {
                List<File> files = new ArrayList<>();
                files.add(new File());
                files.add(new File());
                return files;
            }
        });

        when(fileDAOMock.getById(anyInt())).thenAnswer(new Answer<File>() {
            @Override
            public File answer(InvocationOnMock invocationOnMock) throws Throwable {
                File file = new File();
                file.setId((Integer) invocationOnMock.getArguments()[0]);
                return file;
            }
        });

    }

    @Test(expected = RuntimeException.class)
    public void saveFileWithNotNullProjectTest() throws Exception {
        doThrow(new RuntimeException("Stub save exception")).when(fileDAOMock).save(any(File.class));
        File file = new File();
        Project project = new Project();
        file.setProject(project);
        fileService.save(file);
    }

    @Test
    public void saveFileWithNullProjectTest() throws Exception {
        doThrow(new RuntimeException("Stub save exception")).when(fileDAOMock).save(any(File.class));
        File file = new File();
        try {
            fileService.save(file);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void saveNullFileTest() throws Exception {
        doThrow(new RuntimeException("Stub save exception")).when(fileDAOMock).save(any(File.class));
        try {
            fileService.save(null);
        } catch (Exception e) {
            fail();
        }
    }

    @Test(expected = RuntimeException.class)
    public void deleteNotNullFileTest() throws Exception {
        doThrow(new RuntimeException("Stub delete exception")).when(fileDAOMock).delete(any(File.class));
        File file = new File();
        Project project = new Project();
        file.setProject(project);
        fileService.delete(file);
    }


    @Test
    public void deleteNullFileTest() throws Exception {
        doThrow(new RuntimeException("Stub delete exception")).when(fileDAOMock).delete(any(File.class));
        try {
            fileService.delete(null);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void getFilesByNotNullProjectTest() throws Exception {
        Project project = new Project();
        List<File> files = fileService.getFilesByProject(project);
        assertEquals(2, files.size());
    }

    @Test
    public void getFilesByNullProjectTest() throws Exception {
        List<File> files = fileService.getFilesByProject(null);
        assertEquals(0, files.size());
    }

    @Test
    public void getFilesEntityByNotNullIdTest() throws Exception {
        File file = fileService.getFilesEntityById(1);
        assertEquals(1, file.getId());
    }

    @Test
    public void getFilesEntityByNullIdTest() throws Exception {
        File file = fileService.getFilesEntityById(null);
        assertNull(file);
    }

    @Test (expected = RuntimeException.class)
    public void updateNotNullFileTest() throws Exception {
        doThrow(new RuntimeException()).when(fileDAOMock).update(any(File.class));
        File file = new File();
        fileService.update(file);
    }


    @Test
    public void updateNullFileTest() throws Exception {
        doThrow(new RuntimeException()).when(fileDAOMock).update(any(File.class));
        try {
            fileService.update(null);
        } catch (Exception e) {
            fail();
        }
    }
}