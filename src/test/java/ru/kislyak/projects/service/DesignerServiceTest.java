package ru.kislyak.projects.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import ru.kislyak.projects.dao.DesignerDAO;
import ru.kislyak.projects.pojos.Designer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DesignerServiceTest {
    @Mock
    DesignerDAO designerDAOMock;
    @InjectMocks
    private DesignerService designerService;

    @Before()
    public void addBehaviour() {
        when(designerDAOMock.save(any(Designer.class))).thenAnswer(new Answer<Integer>() {
            @Override
            public Integer answer(InvocationOnMock invocationOnMock) throws Throwable {
                Designer designer = (Designer) invocationOnMock.getArguments()[0];
                designer.setId(1);
                return designer.getId();
            }
        });
    }

    @Test
    public void addNutNullDesignerTest() throws Exception {
        Designer designer = new Designer();
        designer.setLastname("Фамилия");
        int result = designerService.addDesigner(designer);
        assertEquals(1, result);
    }

    @Test
    public void addNullDesignerMustReturnZerroTest() throws Exception {
        Designer designer = new Designer();
        designer.setLastname("Фамилия");
        int result = designerService.addDesigner(null);
        assertEquals(0, result);
    }

    @Test
    public void deleteNotNullDesignerTest() {
        Mockito.doThrow(new RuntimeException("Stub exception")).when(designerDAOMock).delete(any(Designer.class));
        Designer designer = new Designer();
        try {
            designerService.deleteDesigner(designer);
            fail();
        } catch (Exception e) {
            assertEquals("Stub exception", e.getMessage());
        }
    }

    @Test
    public void deleteNullDesignerTest() {
        Mockito.doThrow(new RuntimeException()).when(designerDAOMock).delete(any(Designer.class));
        try {
            designerService.deleteDesigner(null);
        } catch (RuntimeException e) {
            fail();
        }
    }

    @Test
    public void updateNotNullDesignerTest() throws Exception {
        Mockito.doThrow(new RuntimeException("Stub updateProject exception")).when(designerDAOMock).update(any(Designer.class));
        Designer designer = new Designer();
        try {
            designerService.updateDesigner(designer);
            fail();
        } catch (Exception e) {
            assertEquals("Stub updateProject exception", e.getMessage());
        }
    }

    @Test
    public void updateNullDesignerTest() {
        Mockito.doThrow(new RuntimeException("Stub Null updateProject exception")).when(designerDAOMock).update(any(Designer.class));
        try {
            designerService.deleteDesigner(null);
        } catch (RuntimeException e) {
            fail();
        }
    }

    @Test
    public void getDesigners() throws Exception {
        when(designerDAOMock.getAll()).thenAnswer(new Answer<List<Designer>>() {
            @Override
            public List<Designer> answer(InvocationOnMock invocationOnMock) throws Throwable {
                List<Designer> designersMock = new ArrayList<>();
                designersMock.add(new Designer());
                designersMock.add(new Designer());
                return designersMock;
            }
        });
        List<Designer> designers = designerService.getDesigners();
        assertEquals(2, designers.size());
    }
}