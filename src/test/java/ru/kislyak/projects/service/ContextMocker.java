package ru.kislyak.projects.service;

import org.mockito.Mockito;

import javax.faces.context.FacesContext;

abstract class ContextMocker extends FacesContext {
    private ContextMocker() {
    }

    static FacesContext mockFacesContext() {
        FacesContext context = Mockito.mock(FacesContext.class);
        setCurrentInstance(context);
        return context;
    }
}
